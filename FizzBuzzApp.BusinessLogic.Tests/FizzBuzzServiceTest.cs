﻿namespace FizzBuzzApp.BusinessLogic.Tests
{
    using FizzBuzzApp.BusinessLogic;
    using FizzBuzzApp.BusinessLogic.Interfaces;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    using System.Collections.Generic;

    [TestFixture]
    public class FizzBuzzServiceTest
    {
        private Mock<IDivisibleRule> divideByThreeRuleMock;
        private Mock<IDivisibleRule> divideByFiveRuleMock;
        private IEnumerable<IDivisibleRule> divideByRules;
        private FizzBuzzService fizzBuzzService;

        [SetUp]
        public void Setup()
        {
            this.divideByThreeRuleMock = new Mock<IDivisibleRule>();
            this.divideByFiveRuleMock = new Mock<IDivisibleRule>();
            this.divideByRules = new List<IDivisibleRule> 
            { 
                this.divideByThreeRuleMock.Object, 
                this.divideByFiveRuleMock.Object
            };
            this.fizzBuzzService = new FizzBuzzService(this.divideByRules);
        }

        [TestCase(10)]
        [TestCase(24)]
        [TestCase(101)]
        public void GetFizzBuzzItems_TotalCount_ShouldMatchInputValue(int inputValue)
        {
            // Arrange
            this.SetupDivideByMocks();

            // Act
            var fizzBuzzItems = this.fizzBuzzService.GetFizzBuzzItems(inputValue);

            // Assert
            fizzBuzzItems.Count.Should().Be(inputValue);
        }

        [TestCase(21)]
        [TestCase(34)]
        public void GetFizzBuzzItems_ShouldMatch_ExpectedValue(int inputValue)
        {
            // Arrange
            this.SetupDivideByMocks();

            // Act
            var fizzBuzzItems = this.fizzBuzzService.GetFizzBuzzItems(inputValue);

            // Assert
            fizzBuzzItems[1].Should().Be("2");
            fizzBuzzItems[2].Should().Be("fizz");
            fizzBuzzItems[4].Should().Be("buzz");
        }

        private void SetupDivideByMocks()
        {
            this.divideByThreeRuleMock.Setup(x => x.IsDivisible(3)).Returns(true);
            this.divideByThreeRuleMock.Setup(x => x.IsDivisible(15)).Returns(true);
            this.divideByThreeRuleMock.Setup(x => x.GetBusinessValue()).Returns("fizz");
            this.divideByFiveRuleMock.Setup(x => x.IsDivisible(5)).Returns(true);
            this.divideByFiveRuleMock.Setup(x => x.IsDivisible(15)).Returns(true);
            this.divideByFiveRuleMock.Setup(x => x.GetBusinessValue()).Returns("buzz");
        }
    }
}
