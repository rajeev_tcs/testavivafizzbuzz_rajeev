﻿namespace FizzBuzzApp.BusinessLogic.Tests
{
    using FizzBuzzApp.BusinessLogic.Interfaces;
    using FizzBuzzApp.BusinessLogic.Rules;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class DivideByFiveRuleTest
    {
        private Mock<IWeekDayProvider> dayOfWeekMock;
        private DivideByFiveRule divideByFiveRule;

        [SetUp]
        public void Setup()
        {
            this.dayOfWeekMock = new Mock<IWeekDayProvider>();
            this.divideByFiveRule = new DivideByFiveRule(this.dayOfWeekMock.Object);
        }

        [TestCase(5)]
        [TestCase(10)]
        public void IsDivisible_DivideByFive_ShouldReturnTrue(int value)
        {
            // Act
            var isDivisibleByFive = this.divideByFiveRule.IsDivisible(value);

            // Assert
            isDivisibleByFive.Should().BeTrue();
        }

        [TestCase(31)]
        [TestCase(91)]
        [TestCase(301)]
        public void IsDivisible_NotDivideByFive_ShouldReturnFalse(int value)
        {
            // Act
            var isDivisibleByFive = this.divideByFiveRule.IsDivisible(value);

            // Assert
            isDivisibleByFive.Should().BeFalse();
        }

        [TestCase(true, "wuzz")]
        [TestCase(false, "buzz")]
        public void GetBusinessValue_IsBusinessDayOfWeek_ExpectedValue(bool isBusinessDayOfWeek, string expectedValue)
        {
            // Arrange
            this.dayOfWeekMock.Setup(x => x.IsBusinessDayOfWeek()).Returns(isBusinessDayOfWeek);

            // Act
            var actualValue = this.divideByFiveRule.GetBusinessValue();

            // Assert
            actualValue.Should().Be(expectedValue);
        }
    }
}
