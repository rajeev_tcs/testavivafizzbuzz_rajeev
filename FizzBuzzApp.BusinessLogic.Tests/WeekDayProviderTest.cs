﻿namespace FizzBuzzApp.BusinessLogic.Tests
{
    using FizzBuzzApp.BusinessLogic;
    using FizzBuzzApp.BusinessLogic.Interfaces;
    using FluentAssertions;
    using NUnit.Framework;
    using System;

    [TestFixture]
    public class WeekDayProviderTest
    {
        private IWeekDayProvider weekDay;

        [Test]
        public void WeekDayProvider_IsBusinessDayOfWeek_ShouldReturnTrue()
        {
            // Arrange
            this.weekDay = new WeekDayProvider(DateTime.Now.DayOfWeek.ToString());

            // Act
            bool isDayOfWeek = this.weekDay.IsBusinessDayOfWeek();

            // Assert
            isDayOfWeek.Should().BeTrue();
        }

        [Test]
        public void WeekDayProvider_IsNotBusinessDayOfWeek_ShouldReturnFalse()
        {
            // Arrange
            this.weekDay = new WeekDayProvider(DateTime.Now.AddDays(1).DayOfWeek.ToString());

            // Act
            bool isDayOfWeek = this.weekDay.IsBusinessDayOfWeek();

            // Assert
            isDayOfWeek.Should().BeFalse();
        }
    }
}