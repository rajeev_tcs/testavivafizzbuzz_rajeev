﻿namespace FizzBuzzApp.BusinessLogic.Tests
{
    using FizzBuzzApp.BusinessLogic.Interfaces;
    using FizzBuzzApp.BusinessLogic.Rules;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class DivideByThreeRuleTest
    {
        private Mock<IWeekDayProvider> dayOfWeekMock;
        private DivideByThreeRule divideByThreeRule;

        [SetUp]
        public void Setup()
        {
            this.dayOfWeekMock = new Mock<IWeekDayProvider>();
            this.divideByThreeRule = new DivideByThreeRule(this.dayOfWeekMock.Object);
        }

        [TestCase(3)]
        [TestCase(9)]
        [TestCase(30)]
        public void IsDivisible_DivideByThree_ShouldReturnTrue(int value)
        {
            // Act
            var isDivisibleByThree = this.divideByThreeRule.IsDivisible(value);

            // Assert
            isDivisibleByThree.Should().BeTrue();
        }

        [TestCase(31)]
        [TestCase(91)]
        [TestCase(301)]
        public void IsDivisible_NotDivideByThree_ShouldReturnFalse(int value)
        {
            // Act
            var isDivisibleByThree = this.divideByThreeRule.IsDivisible(value);

            // Assert
            isDivisibleByThree.Should().BeFalse();
        }

        [TestCase(true, "wizz")]
        [TestCase(false, "fizz")]
        public void GetBusinessValue_IsBusinessDayOfWeek_ExpectedValue(bool isBusinessDayOfWeek, string expectedValue)
        {
            // Arrange
            this.dayOfWeekMock.Setup(x => x.IsBusinessDayOfWeek()).Returns(isBusinessDayOfWeek);

            // Act
            var actualValue = this.divideByThreeRule.GetBusinessValue();

            // Assert
            actualValue.Should().Be(expectedValue);
        }
    }
}
