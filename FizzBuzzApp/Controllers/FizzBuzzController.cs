﻿namespace FizzBuzzApp.Controllers
{
    using FizzBuzzApp.BusinessLogic.Interfaces;
    using FizzBuzzApp.Models.FizzBuzz;
    using PagedList;
    using System.Web.Mvc;

    public class FizzBuzzController : Controller
    {
        private const int PAGE_SIZE = 20;
        private readonly IFizzBuzzService fizzBuzzService;

        /// <summary>
        /// Constructor for FizzBuzzController.
        /// </summary>
        /// <param name="service">Service to get FizzBuzz items</param>
        public FizzBuzzController(IFizzBuzzService service)
        {
            this.fizzBuzzService = service;
        }

        /// <summary>
        /// Home
        /// </summary>
        /// <returns>View</returns>
        [HttpGet]
        public ActionResult Home()
        {
            return View("Home");
        }

        /// <summary>
        /// Home
        /// </summary>
        /// <param name="model">FizzBuzz model object</param>
        /// <returns>Home view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Home(FizzBuzzModel model)
        {
            if (ModelState.IsValid)
            {
                model.FizzBuzzList = this.GetListByPage(model.UserValue);
                return View("Home", model);
            }

            return View("Home");
        }

        /// <summary>
        /// Navigate to given page number.
        /// </summary>
        /// <param name="value">Input value</param>
        /// <param name="page">Page number</param>
        /// <returns>Home view</returns>
        [HttpGet]
        public ActionResult NavigateToPage(int value, int? page)
        {
            var model = new FizzBuzzModel
            {
                UserValue = value,
                FizzBuzzList = this.GetListByPage(value, page)
            };

            return View("Home", model);
        }

        /// <summary>
        /// Gets FizzBuzz list of items by page.
        /// </summary>
        /// <param name="value">Input value</param>
        /// <param name="page">Page number</param>
        /// <returns>Paged list</returns>
        private IPagedList<string> GetListByPage(int value, int? page = 1)
        {
            return this.fizzBuzzService.GetFizzBuzzItems(value).ToPagedList(page ?? 1, PAGE_SIZE);
        }
    }
}