﻿namespace FizzBuzzApp.Models.FizzBuzz
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using PagedList;

    /// <summary>
    /// FizzBuzzViewModel for the FizzBuzz Application
    /// </summary>
    public class FizzBuzzModel
    {
        [Required(ErrorMessage = "The value is mandatory.")]
        [DisplayName("Input Value ")]
        [Range(1, 1000, ErrorMessage = "The value should be an integer between 1 and 1000.")]
        public int UserValue { get; set; }

        public IPagedList<string> FizzBuzzList { get; set; }
    }
}