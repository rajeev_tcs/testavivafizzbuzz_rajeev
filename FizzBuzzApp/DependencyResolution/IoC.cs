namespace FizzBuzzApp {
    using FizzBuzzApp.BusinessLogic;
    using FizzBuzzApp.BusinessLogic.Interfaces;
    using FizzBuzzApp.BusinessLogic.Rules;
    using StructureMap;
    using System.Configuration;
    public static class IoC {
        public static IContainer Initialize() {
            ObjectFactory.Initialize(x =>
                        {
                            x.Scan(scan =>
                                    {
                                        scan.TheCallingAssembly();
                                        scan.WithDefaultConventions();
                                    });
                            x.For<IFizzBuzzService>().Use<FizzBuzzService>();
                            x.For<IDivisibleRule>().Use<DivideByThreeRule>();
                            x.For<IDivisibleRule>().Use<DivideByFiveRule>();
                            x.For<IWeekDayProvider>().Use<WeekDayProvider>()
                                .Ctor<string>("dayOfWeek").Is(ConfigurationManager.AppSettings["BusinessDayOfWeek"].ToString());
                        });
            return ObjectFactory.Container;
        }
    }
}