﻿namespace FizzBuzzApp.Tests
{
    using FizzBuzzApp.BusinessLogic.Interfaces;
    using FizzBuzzApp.Controllers;
    using FizzBuzzApp.Models.FizzBuzz;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Web.Mvc;

    [TestFixture]
    public class FizzBizzControllerTest
    {
        private Mock<IFizzBuzzService> fizzBuzzServiceMock;
        private FizzBuzzController fizzBuzzController;

        [SetUp]
        public void Setup()
        {
            this.fizzBuzzServiceMock = new Mock<IFizzBuzzService>();
            fizzBuzzController = new FizzBuzzController(fizzBuzzServiceMock.Object);
        }

        [Test]
        public void Home_PageLoad_ShouldReturnView()
        {
            // Act
            var result = this.fizzBuzzController.Home() as ViewResult;

            // Assert
            result.Should().NotBeNull();
            result.ViewName.Should().Be("Home");
        }

        [Test]
        public void Home_ValidModelState_ShouldReturnExptectedValues()
        {
            // Arrange
            var result = new List<string>() { "1", "2", "fizz", "4" };
            var model = new FizzBuzzModel { UserValue = 4, FizzBuzzList = null };
            this.fizzBuzzServiceMock.Setup(x => x.GetFizzBuzzItems(4)).Returns(result);

            // Act
            var actualResult = this.fizzBuzzController.Home(model) as ViewResult;
            var actualModel = actualResult.Model as FizzBuzzModel;

            // Assert
            actualResult.ViewName.Should().Be("Home");
            actualModel.Should().NotBeNull();
            actualModel.FizzBuzzList[2].Should().Be("fizz");
            actualModel.FizzBuzzList.Count.Should().Be(4);
        }

        [Test]
        public void Home_InValidModelState_ShouldReturnExptectedValues()
        {
            // Arrange
            var result = new List<string>() { "1", "2" };
            var model = new FizzBuzzModel { UserValue = 2, FizzBuzzList = null };
            this.fizzBuzzServiceMock.Setup(x => x.GetFizzBuzzItems(2)).Returns(result);
            this.fizzBuzzController.ModelState.AddModelError("Input Value", "The value is mandatory.");

            // Act
            var actualResult = this.fizzBuzzController.Home(model) as ViewResult;
            var actualModel = actualResult.Model as FizzBuzzModel;

            // Assert
            actualResult.ViewName.Should().Be("Home");
            actualModel.Should().BeNull();
        }

        [Test]
        public void NavigateToPage_FizzBuzzItems_ShouldReturnExptectedValues()
        {
            // Arrange
            var result = new List<string>() { "1", "2", "fizz", "4" };
            this.fizzBuzzServiceMock.Setup(x => x.GetFizzBuzzItems(4)).Returns(result);

            // Act
            var actualResult = this.fizzBuzzController.NavigateToPage(4, 1) as ViewResult;
            var actualModel = actualResult.Model as FizzBuzzModel;

            // Assert
            actualResult.ViewName.Should().Be("Home");
            actualModel.Should().NotBeNull();
            actualModel.FizzBuzzList[1].Should().Be("2");
            actualModel.FizzBuzzList[2].Should().Be("fizz");
            actualModel.FizzBuzzList.Count.Should().Be(4);
        }
    }
}