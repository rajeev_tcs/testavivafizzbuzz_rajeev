﻿namespace FizzBuzzApp.Tests
{
    using FizzBuzzApp.Models.FizzBuzz;
    using FluentAssertions;
    using NUnit.Framework;
    using PagedList;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [TestFixture]
    public class FizzBuzzModelTest
    {
        private FizzBuzzModel fizzBuzzModel;

        [TestCase(0, false)]
        [TestCase(21, true)]
        [TestCase(-9, false)]
        [TestCase(1001, false)]
        public void UserValue_Input_MustBePositiveInteger(int userInput, bool expectedResult)
        {
            // Arrange
            fizzBuzzModel = new FizzBuzzModel
            {
                UserValue = userInput,
                FizzBuzzList = new List<string> { "1", "2" }.ToPagedList(1, 20)
            };

            // Act
            var validationErrors = ValidateModel(fizzBuzzModel);

            //Assert
            (validationErrors.Count <= 0).Should().Be(expectedResult);
        }

        private IList<ValidationResult> ValidateModel(FizzBuzzModel model)
        {
            var result = new List<ValidationResult>();
            var validationContext = new ValidationContext(model, null, null);
            validationContext.MemberName = "UserValue";
            Validator.TryValidateProperty(model.UserValue, validationContext, result);
            return result;
        }
    }
}