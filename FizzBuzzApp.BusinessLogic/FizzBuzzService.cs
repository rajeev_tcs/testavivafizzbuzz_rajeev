﻿namespace FizzBuzzApp.BusinessLogic
{
    using System.Collections.Generic;
    using System.Linq;
    using FizzBuzzApp.BusinessLogic.Interfaces;

    public class FizzBuzzService : IFizzBuzzService
    {
        private readonly IEnumerable<IDivisibleRule> divideByRuleList;

        public FizzBuzzService(IEnumerable<IDivisibleRule> divideByRules)
        {
            this.divideByRuleList = divideByRules;
        }

        public IList<string> GetFizzBuzzItems(int userInput)
        {
            var resultItems = new List<string>();
            for (int counter = 1; counter <= userInput; counter++)
            {
                var items = this.divideByRuleList.Where(x => x.IsDivisible(counter)).ToList();
                var resultValue = string.Empty;
                resultValue = items.Any() ? string.Join(" ", items.Select(x => x.GetBusinessValue())) : counter.ToString();
                resultItems.Add(resultValue);
            }

            return resultItems;
        }
    }
}