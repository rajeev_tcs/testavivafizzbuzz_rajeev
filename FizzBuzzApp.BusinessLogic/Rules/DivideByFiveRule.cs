﻿namespace FizzBuzzApp.BusinessLogic.Rules
{
    using FizzBuzzApp.BusinessLogic.Interfaces;

    public class DivideByFiveRule : IDivisibleRule
    {
        private readonly IWeekDayProvider weekDay;

        public DivideByFiveRule(IWeekDayProvider dayOfWeek)
        {
            this.weekDay = dayOfWeek;
        }

        public bool IsDivisible(int value)
        {
            return value % 5 == 0;
        }

        public string GetBusinessValue()
        {
            return this.weekDay.IsBusinessDayOfWeek() ? "wuzz" : "buzz";
        }
    }
}