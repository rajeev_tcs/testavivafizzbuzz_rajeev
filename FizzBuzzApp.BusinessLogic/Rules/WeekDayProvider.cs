﻿namespace FizzBuzzApp.BusinessLogic
{
    using System;
    using FizzBuzzApp.BusinessLogic.Interfaces;

    public class WeekDayProvider : IWeekDayProvider
    {
        private readonly string weekDay;

        public WeekDayProvider(string dayOfWeek)
        {
            this.weekDay = dayOfWeek;
        }

        public bool IsBusinessDayOfWeek()
        {
            return this.weekDay.Equals(DateTime.Today.DayOfWeek.ToString());
        }
    }
}