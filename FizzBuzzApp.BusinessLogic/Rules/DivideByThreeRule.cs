﻿namespace FizzBuzzApp.BusinessLogic.Rules
{
    using FizzBuzzApp.BusinessLogic.Interfaces;

    public class DivideByThreeRule : IDivisibleRule
    {
        private readonly IWeekDayProvider weekDay;

        public DivideByThreeRule(IWeekDayProvider dayOfWeek)
        {
            this.weekDay = dayOfWeek;
        }

        public bool IsDivisible(int value)
        {
            return value % 3 == 0;
        }

        public string GetBusinessValue()
        {
            return this.weekDay.IsBusinessDayOfWeek() ? "wizz" : "fizz";
        }
    }
}