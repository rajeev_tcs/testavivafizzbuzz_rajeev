﻿namespace FizzBuzzApp.BusinessLogic.Interfaces
{
    public interface IDivisibleRule
    {
        bool IsDivisible(int value);

        string GetBusinessValue();
    }
}