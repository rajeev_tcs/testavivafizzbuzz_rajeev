﻿namespace FizzBuzzApp.BusinessLogic.Interfaces
{
    public interface IWeekDayProvider
    {
        bool IsBusinessDayOfWeek();
    }
}