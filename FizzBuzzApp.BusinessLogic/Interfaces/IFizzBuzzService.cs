﻿namespace FizzBuzzApp.BusinessLogic.Interfaces
{
    using System.Collections.Generic;

    public interface IFizzBuzzService
    {
        IList<string> GetFizzBuzzItems(int userInput);
    }
}